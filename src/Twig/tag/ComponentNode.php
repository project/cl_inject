<?php

namespace Drupal\cl_inject\Twig\tag;

use Drupal\cl_inject\Exception\ComponentSyntaxException;
use Twig\Compiler;
use Twig\Node\Expression\AbstractExpression;
use Twig\Node\Expression\ConstantExpression;
use Twig\Node\Node;
use Twig\Node\NodeOutputInterface;

/**
 * Generates the PHP code from the parsed template.
 */
class ComponentNode extends Node implements NodeOutputInterface {

  /**
   * ComponentNode constructor.
   *
   * @param \Twig\Node\Expression\AbstractExpression $expr
   *   The expression.
   * @param \Twig\Node\Expression\ConstantExpression $variant
   *   The variant.
   * @param \Twig\Node\Expression\AbstractExpression|null $variables
   *   The variables.
   * @param int $lineno
   *   The line number.
   * @param string|null $tag
   *   The tag name.
   */
  public function __construct(
    AbstractExpression $expr,
    ConstantExpression $variant,
    ?AbstractExpression $variables,
    ?Node $children,
    int $lineno,
    string $tag = NULL
  ) {
    $nodes = ['expr' => $expr];

    if ($variant->getAttribute('value') !== '') {
      $nodes['variant'] = $variant;
    }

    if ($variables !== NULL) {
      $nodes['variables'] = $variables;
    }

    $nodes['children'] = $children;

    parent::__construct($nodes, [], $lineno, $tag);
  }

  /**
   * Compiles.
   *
   * @param \Twig\Compiler $compiler
   *   The compiler.
   *
   * @throws \Drupal\cl_inject\Exception\ComponentSyntaxException
   */
  public function compile(Compiler $compiler): void {
    $compiler->addDebugInfo($this);
    $counter = $this->getAttribute('counter');

    $expr_node = $this->getNode('expr');
    if (!$expr_node instanceof ConstantExpression) {
      throw new ComponentSyntaxException("Use quoted strings for the {% component 'my-component' %} tag.");
    }
    $component_name = $expr_node->getAttribute('value');

    // $variant = 'my-variant';
    $compiler->write(sprintf('$_variant[%s] = ', $counter));
    $this->hasNode('variant')
      ? $compiler->subcompile($this->getNode('variant'))
      : $compiler->raw("''");
    $compiler->write(';')->raw(PHP_EOL);

    if ($counter > 1) {
      $compiler->write(sprintf(
        '$context = array_merge($context, $_props[%d] ?? []);',
        $counter - 1
      ))->raw(PHP_EOL);
    }
    // $props = ['foo' => 'bar'];
    $compiler->write(sprintf('$_props[%s] = ', $counter));
    $this->hasNode('variables')
      ? $compiler->subcompile($this->getNode('variables'))
      : $compiler->raw('[]');
    $compiler->write(';')->raw(PHP_EOL);

    if (!$this->hasNode('children')) {
      $compiler->write(sprintf('$_children[%d] = "";', $counter));
    }
    elseif ($this->getNode('children') instanceof AbstractExpression) {
      $compiler->write(sprintf('$_children[%d] = ', $counter))
        ->subcompile($this->getNode('children'))
        ->raw(';')
        ->raw(PHP_EOL);
    }
    else {
      $compiler->write('ob_start();')->raw(PHP_EOL);
      $compiler->subcompile($this->getNode('children'));
      $compiler->write(sprintf('$_children[%d] = ob_get_clean();', $counter))
        ->raw(PHP_EOL);
    }

    // Get the service.
    $compiler->raw('$renderer = $this->extensions[')
      ->string("Drupal\cl_inject\Twig\TwigExtension")
      ->write(']->getRenderer();')->raw(PHP_EOL);

    // Echo the results of the render.
    $compiler->raw('echo ')
      ->write('$renderer->renderComponent(')
      ->string($component_name)
      ->raw(sprintf(', $_props[%d]', $counter))
      ->raw(sprintf(', $_variant[%d]', $counter))
      ->raw(sprintf(', $_children[%d]', $counter))
      ->raw(', $context')
      ->raw(');')
      ->raw(PHP_EOL);
  }

}

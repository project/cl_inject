<?php

namespace Drupal\cl_inject\Twig\tag;

use Twig\Error\SyntaxError;
use Twig\Node\Expression\ConstantExpression;
use Twig\Token;
use Twig\TokenParser\AbstractTokenParser;

/**
 * Parses the template syntax.
 */
class ComponentTokenParser extends AbstractTokenParser {

  /**
   * Opening twig tag.
   *
   * @var string
   */
  private string $tagName;

  /**
   * Opening twig tag.
   *
   * @var string
   */
  private string $endTag;

  /**
   * Creates a new ComponentTokenParser.
   *
   * @param string $tag_name
   *   The name of the tag.
   */
  public function __construct(string $tag_name = 'component') {
    $this->tagName = $tag_name;
    $this->endTag = sprintf('end%s', $tag_name);
  }

  /**
   * Parses the Component token.
   *
   * @param \Twig\Token $token
   *   The token.
   *
   * @return ComponentNode
   *   The node.
   *
   * @throws \Twig\Error\SyntaxError
   */
  public function parse(Token $token): ComponentNode {
    $lineno = $token->getLine();
    $stream = $this->parser->getStream();
    // Recovers all inline parameters close to your tag name.
    [
      $component_name,
      $variant,
      $props,
    ] = array_merge([], $this->parseArguments($token));
    $this->parser->pushLocalScope();

    $continue = TRUE;
    $children = NULL;
    while ($continue) {
      // Create subtree until the decideComponentFork() callback returns true.
      $children = $this->parser->subparse([$this, 'decideComponentFork']);

      // I like to put a switch here, in case you need to add middle tags, such
      // as: {% inject %}, {% endinject %}.
      $tag = $stream->next()->getValue();

      switch ($tag) {
        case $this->endTag:
          $continue = FALSE;
          break;

        default:
          throw new SyntaxError(sprintf('Unexpected end of template. Twig was looking for the following tags "endinject" to close the "inject" block started at line %d)', $lineno), -1);
      }
      $this->parser->popLocalScope();

      // Parse the name for the {% endinject 'my-component' %}.
      $end_component_name = $this->parseEndComponentName($token);
      $correct_closure = $end_component_name->getAttribute('value') === $component_name->getAttribute('value');
      if (!$correct_closure) {
        $message = sprintf(
          'The name in the closing tag ("%s") does not match the component name ("%s") at line %d of the template.',
          $end_component_name->getAttribute('value'),
          $component_name->getAttribute('value'),
          $token->getLine()
        );
        throw new SyntaxError($message);
      }
    }

    return new ComponentNode(
      $component_name,
      $variant,
      $props,
      $children,
      $lineno,
      $this->getTag()
    );
  }

  /**
   * Parse the endinject name.
   *
   * @param \Twig\Token $token
   *   The token.
   *
   * @return \Twig\Node\Expression\ConstantExpression
   *   The name of the closing tag.
   *
   * @throws \Twig\Error\SyntaxError
   */
  protected function parseEndComponentName(Token $token): ConstantExpression {
    $stream = $this->parser->getStream();
    if ($stream->getCurrent()->test(Token::BLOCK_END_TYPE)) {
      $message = sprintf('Invalid closing tag at line %d. You need to provide the name of the component you are closing. Example: {%% endinject \'my-component\' %%}.', $token->getLine());
      throw new SyntaxError($message);
    }
    $name = $this->parser->getExpressionParser()->parseExpression();
    $stream->expect(Token::BLOCK_END_TYPE);
    if (!$name instanceof ConstantExpression) {
      $message = sprintf('Invalid closing tag for "endinject" at line %d. Make sure to match the component name {%% endinject \'my-component\' %%}.', $token->getLine());
      throw new SyntaxError($message);
    }
    return $name;
  }

  /**
   * Parses the arguments.
   *
   * @return array
   *   The parsed arguments.
   *
   * @throws \Twig\Error\SyntaxError
   */
  protected function parseArguments(): array {
    $stream = $this->parser->getStream();

    $variables = NULL;
    $variant = new ConstantExpression('', 0);

    $component_name = $this->parser->getExpressionParser()
      ->parseStringExpression();
    if ($stream->nextIf(Token::NAME_TYPE, 'variant')) {
      $variant = $this->parser->getExpressionParser()->parseStringExpression();
    }
    if ($stream->nextIf(Token::NAME_TYPE, 'with')) {
      $variables = $this->parser->getExpressionParser()->parseExpression();
    }

    $stream->expect(Token::BLOCK_END_TYPE);
    return [$component_name, $variant, $variables];
  }

  /**
   * Get the tag name.
   *
   * @return string
   *   The tag name.
   */
  public function getTag(): string {
    return $this->tagName;
  }

  /**
   * Callback called at each tag name when subparsing.
   *
   * Must return true when the expected end tag is reached.
   *
   * @param \Twig\Token $token
   *   The token.
   *
   * @return bool
   *   TRUE if we need to start a subparse job.
   */
  public function decideComponentFork(Token $token): bool {
    return $token->test([$this->endTag]);
  }

}

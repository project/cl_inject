<?php

namespace Drupal\cl_inject\Twig;

use Drupal\cl_inject\Twig\tag\ComponentNode;
use Twig\Environment;
use Twig\Node\Node;
use Twig\NodeVisitor\NodeVisitorInterface;

/**
 * Node visitor for the components.
 */
class ComponentNodeVisitor implements NodeVisitorInterface {

  /**
   * The internal counter.
   *
   * @var int
   */
  private int $counter = 0;

  /**
   * {@inheritdoc}
   */
  public function enterNode(Node $node, Environment $env) {
    if ($node instanceof ComponentNode) {
      $node->setAttribute('counter', $this->counter++);
    }
    return $node;
  }

  /**
   * {@inheritdoc}
   */
  public function leaveNode(Node $node, Environment $env) {
    if ($node instanceof ComponentNode) {
      $node->setAttribute('counter', $this->counter--);
    }
    return $node;
  }

  /**
   * {@inheritdoc}
   */
  public function getPriority(): int {
    return 0;
  }

}

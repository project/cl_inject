<?php

namespace Drupal\cl_inject\Twig;

use Drupal\cl_inject\Service\ComponentRenderer;
use Drupal\cl_inject\Twig\tag\ComponentTokenParser;
use Twig\Compiler;
use Twig\Extension\AbstractExtension;

/**
 * The twig extension so Drupal can recognize the new code.
 */
class TwigExtension extends AbstractExtension {

  /**
   * The renderer.
   *
   * @var \Drupal\cl_inject\Service\ComponentRenderer
   */
  private ComponentRenderer $renderer;

  /**
   * TwigComponentExtension constructor.
   *
   * @param \Drupal\cl_inject\Service\ComponentRenderer $component_renderer
   *   The component renderer.
   */
  public function __construct(ComponentRenderer $component_renderer) {
    $this->renderer = $component_renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function getTokenParsers(): array {
    return [new ComponentTokenParser('inject')];
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeVisitors(): array {
    return [new ComponentNodeVisitor()];
  }

  /**
   * Gets the renderer.
   *
   * @return \Drupal\cl_components\Service\ComponentRenderer
   *   The renderer.
   */
  public function getRenderer(): ComponentRenderer {
    return $this->renderer;
  }

  /**
   * Creates a Twig compiler.
   *
   * @return \Twig\Compiler
   *   The compiler.
   */
  public function createCompiler(): Compiler {
    return new Compiler($this->renderer->getTwigEnvironment());
  }

}

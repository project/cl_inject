<?php

namespace Drupal\cl_inject\Service;

use Drupal\cl_components\Component\Component;
use Drupal\cl_components\Component\ComponentDiscovery;
use Drupal\cl_components\Exception\InvalidComponentException;
use Drupal\cl_components\Exception\TemplateNotFoundException;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\RendererInterface;
use JsonSchema\Validator;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Render a component with some context.
 */
class ComponentRenderer {

  /**
   * The twig environment.
   *
   * @var \Twig\Environment
   */
  private Environment $environment;

  /**
   * The component discovery.
   *
   * @var \Drupal\cl_components\Component\ComponentDiscovery
   */
  private ComponentDiscovery $componentDiscovery;

  /**
   * The Drupal renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private RendererInterface $drupalRenderer;

  /**
   * The target.
   *
   * @var array
   */
  private array $target = ['context' => [], 'variant' => ''];

  /**
   * Creates a new ComponentRenderer.
   *
   * @param \Drupal\Core\Render\RendererInterface $drupal_renderer
   *   The drupal renderer.
   * @param \Drupal\cl_components\Component\ComponentDiscovery $component_discovery
   *   The discovery.
   */
  public function __construct(RendererInterface $drupal_renderer, ComponentDiscovery $component_discovery) {
    $this->drupalRenderer = $drupal_renderer;
    $this->componentDiscovery = $component_discovery;
  }

  /**
   * Renders the Twig markup of a component.
   *
   * @param string $name
   *   The component to render.
   * @param array $context
   *   The context of the component.
   * @param string $variant
   *   The variant.
   *
   * @return string
   *   The rendered markup.
   *
   * @throws \Drupal\cl_components\Exception\ComponentNotFoundException
   * @throws \Drupal\cl_components\Exception\TemplateNotFoundException
   */
  public function renderTemplate(string $name, array $context = [], string $variant = ''): string {
    $component = $this->componentDiscovery->find($name);
    if (!empty($variant) && !in_array($variant, $component->getVariants())) {
      $message = sprintf(
        'Unable to render variant "%s". This variant is not declared in the metadata.json for this component.',
        $variant
      );
      throw new TemplateNotFoundException($message);
    }
    $context = array_merge(
      $context,
      $component->additionalRenderContext($variant)
    );
    $component_path = $component->getMetadata()->getPath();
    $args = [$component_path, DIRECTORY_SEPARATOR, $component->getId()];
    $template_path = empty($variant)
      ? sprintf('%s%s%s.twig', ...$args)
      : sprintf('%s%s%s--%s.twig', ...[...$args, $variant]);
    try {
      $template_wrapper = $this->environment->load($template_path);
    }
    catch (LoaderError | RuntimeError | SyntaxError $e) {
      watchdog_exception('cl_inject', $e);
      return '';
    }
    return $template_wrapper->render($context);
  }

  /**
   * Validates the provided props against the schema.
   *
   * @param array $props
   *   The props from the render array or template.
   * @param \Drupal\cl_components\Component\Component $component
   *   The component object.
   *
   * @throws \Drupal\cl_components\Exception\InvalidComponentException
   */
  private function validateProps(array $props, Component $component): void {
    $validator = new Validator();
    $prop_schema = $component->getMetadata()->getSchemas()['props'];
    // Add an exception for XDEBUG_TRIGGER, so we can debug more comfortably.
    $prop_schema['properties']['XDEBUG_TRIGGER'] = ['type' => 'string'];
    $schema = Validator::arrayToObjectRecursive($prop_schema);
    if (!is_object($schema)) {
      throw new InvalidComponentException('Unable to find schema for component props.');
    }
    $object_props = Validator::arrayToObjectRecursive($props);
    $validator->validate($object_props, (object) $schema);
    if (!$validator->isValid()) {
      $message_parts = array_map(
        fn(array $error): string => sprintf("[%s] %s", $error['property'], $error['message']),
        $validator->getErrors()
      );
      $message = implode("/n", $message_parts);
      throw new InvalidComponentException($message);
    }
  }

  /**
   * Renders the Twig markup of a component.
   *
   * @param string $name
   *   The component to render.
   * @param array $props
   *   The context of the component.
   * @param string $variant
   *   The variant.
   * @param string $children
   *   The inner HTML for the component.
   * @param array $context
   *   Additional context for the template.
   *
   * @return array
   *   The rendered markup.
   *
   * @throws \Drupal\cl_components\Exception\ComponentNotFoundException
   * @throws \Drupal\cl_components\Exception\InvalidComponentException
   * @throws \Drupal\cl_components\Exception\TemplateNotFoundException
   * @throws \Twig\Error\LoaderError
   * @throws \Twig\Error\SyntaxError
   */
  public function createRenderArray(string $name, array $props = [], string $variant = '', string $children = '', array $context = []): array {
    $component = $this->componentDiscovery->find($name);
    $children = $props['children'] ?? $children;
    // This will allow rendering the HTML and the components inside.
    $raw_children = "{% autoescape false %}\n  $children\n{% endautoescape %}";
    $children = trim(
      $this->getTwigEnvironment()
        ->createTemplate($raw_children)
        ->render($props)
    );
    $this->validateProps($props, $component);
    try {
      $rendered = $this->renderTemplate(
        $component->getId(),
        array_merge($context, $props),
        $variant
      );
    }
    catch (RuntimeError $e) {
      watchdog_exception('cl_components', $e);
      return ['#markup' => ''];
    }
    return [
      '#markup' => Markup::create($rendered),
      '#attached' => ['library' => [$component->getLibraryName()]],
    ];
  }

  /**
   * Renders the component.
   *
   * @param string $name
   *   The component to render.
   * @param array $props
   *   The context of the component.
   * @param string $variant
   *   The variant.
   * @param string $children
   *   The inner HTML for the component.
   * @param array $context
   *   Additional context for the template.
   *
   * @return \Drupal\Component\Render\MarkupInterface|string|mixed
   *   The rendered markup.
   *
   * @throws \Exception
   */
  public function renderComponent(string $name, array $props = [], string $variant = '', string $children = '', array $context = []) {
    $elements = $this->createRenderArray($name, $props, $variant, $children, $context);
    return $this->drupalRenderer->render($elements);
  }

  /**
   * Gets the context.
   *
   * @return array
   *   The context.
   */
  public function getContext(): array {
    return $this->target['context'] ?? [];
  }

  /**
   * Gets the Twig environment.
   *
   * @returns \Twig\Environment
   *   The environment.
   */
  public function getTwigEnvironment(): Environment {
    return $this->environment;
  }

  /**
   * Sets the Twig environment.
   *
   * @param \Twig\Environment $environment
   *   The environment.
   */
  public function setTwigEnvironment(Environment $environment): void {
    $this->environment = $environment;
  }

}

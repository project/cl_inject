<?php

namespace Drupal\cl_inject\Exception;

/**
 * Component syntax exception.
 */
class ComponentSyntaxException extends \Exception {}
